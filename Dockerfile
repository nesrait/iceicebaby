FROM alpine:3.2

RUN \
  apk update \
  && apk add ca-certificates coreutils util-linux  \
  && update-ca-certificates

ADD labels.csv /tmp/labels.csv

VOLUME /input
VOLUME /output

CMD sh
