
https://www.theice.com/marketdata/reports/180
https://www.theice.com/marketdata/reports/icebenchmarkadmin/ISDAFIXHistoricalRates.shtml

docker build -t alpine-csv .

docker run -it -v $PWD/input:/input -v $PWD/output:/output alpine-csv sh

# Scripts

DOWNLOAD_URL="https://www.theice.com/marketdata/reports/icebenchmarkadmin/ISDAFIXHistoricalRates.shtml?excelExport=&criteria.seriesName=EUR+Rates&criteria.runCode=1100"

#YEAR=16; MONTH=10; DAY=5
#SDAY=`printf "%02d" $DAY`; SMONTH=`printf "%02d" $MONTH`; SYEAR=`printf "%02d" $YEAR`
#wget "${DOWNLOAD_URL}&criteria.reportDate=${SMONTH}%2F${SDAY}%2F${SYEAR}" -O /input/20${SYEAR}-${SMONTH}-${SDAY}.csv

input_start="2014-08-01"
input_end="2015-12-31"

# Credits for the date iteration logic
# http://stackoverflow.com/questions/28226229/bash-looping-through-dates
startdate=$(date -I -d "$input_start") || exit -1
enddate=$(date -I -d "$input_end")     || exit -1

d="$startdate"
while [ "$d" != "$enddate" ]; do 
  SYEAR=`date -d $d +%y`
  SMONTH=`date -d $d +%m`
  SDAY=`date -d $d +%d`
  echo "Downloading data for $d..."
  wget "${DOWNLOAD_URL}&criteria.reportDate=${SMONTH}%2F${SDAY}%2F${SYEAR}" -O /input/20${SYEAR}-${SMONTH}-${SDAY}.csv
  d=$(date -I -d "$d + 1 day")
done

paste /tmp/labels.csv /input/*.csv | sed -r 's/Tenor," EUR Rates 1100 ([^"]+)"/\1/g' | sed -r 's/(    )//g' | sed -r 's/\t\d+ Years?,/\t/g' > /output/ICEBaby.csv
